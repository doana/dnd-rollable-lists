# DnD Rollable Lists

A simple extension that allows you to roll randomly on the various lists available on dndbeyond.com including: 

* [Spells](https://www.dndbeyond.com/spells)
* [Monsters](https://www.dndbeyond.com/monsters)
* [Feats](https://www.dndbeyond.com/feats)
* [Equipment](https://www.dndbeyond.com/equipment)
* [Magic Items](https://www.dndbeyond.com/magic-items)
* [Backgrounds](https://www.dndbeyond.com/backgrounds)

When on any of these pages, it adds a small dice icon in the top right of the browser window. Clicking this icon will randomly select an item from the list. Version 1.3 adds support for pagers. Click and hold the button to pick an item from a random page in your list.

The extensions supports the filters provided by dndbeyond, so you can filter your list as you see fit (E.g. filter spells to a specific class or  level) and it will repsect those filters when picking an item. 