whitelist_urls = [
    '^\/spells.*',
    '^\/spells\/class.*',
    '^\/monsters.*', 
    '^\/feats.*',
    '^\/equipment.*',
    '^\/magic-items.*',
    '^\/backgrounds.*'
]

const styles = `
    #dnd-rollable-lists {
        padding: 1em;
        position: fixed;
        top: 0;
        right: 0;
    }

    #dnd-rollable-lists-button {
        background-color: transparent;
        border: none;
        transition: filter 0.5s;
    }

    #dnd-rollable-lists-button:hover,
    #dnd-rollable-lists-button:focus {
        filter: hue-rotate(90deg);
    }

    .randomly-selected {
        background-color: #045ABF !important;
    }

    .grow {
        animation: grow 4s infinite;
    }

    @keyframes grow {
        0% {
            transform: scale(1);
        }

        50% {
            transform: scale(1.2);
        }
            
        100% {
            transform: scale(1.3);
        }
    }


`

const enabled = is_enabled();

if (enabled === true) {
    inject_button();
    inject_styles();

    // If we're part way through a paged selection, pick an item in the page.
    paged_pick();
}

function inject_styles() {
    const style_element = document.createElement("style");
    style_element.innerText = styles;
    document.head.appendChild(style_element);
}

function inject_button() {
    const wrapper = document.createElement('div');
    wrapper.id = "dnd-rollable-lists";

    const button = document.createElement('button');
    button.id = 'dnd-rollable-lists-button';
    button.onclick = get_random;
    button.onmousedown = mousedown_handler;
    button.onmouseleave = mouseexit_handler;
    button.onmouseup = mouseexit_handler;

    wrapper.appendChild(button);

    const icon_url = chrome.runtime.getURL("icons/d8-colour.png");
    const icon = document.createElement('img');
    icon.src = icon_url;
    icon.alt = "Click to pick someting at random! Click and hold to pick something from any page.";
    button.appendChild(icon);    

    const parent = document.getElementById('content');
    parent.appendChild(wrapper);
}


let timeout_id = 0;

/**
 * Handler for long click on button
 */
function mousedown_handler() {
    timeout_id = setTimeout(paged_get_random, 2000);
    const button = document.getElementById('dnd-rollable-lists-button');
    button.classList.add('grow');
}

/**
 * Handler for clearing the long click timer.
 */
function mouseexit_handler() {
    clearTimeout(timeout_id);
    const button = document.getElementById('dnd-rollable-lists-button');
    button.classList.remove('grow');
}

/**
 * Tests to see if we're in the middle of a selection session, 
 * and if so picks a list item and cleans up the session
 */
function paged_pick() {
    if(session_exists()) {
        const random_item = get_random_item();
        random_item.classList.add('randomly-selected');
        random_item.scrollIntoView();

        end_session();
    }
}

/**
 * Pick a random page and navigate there. 
 */
function goto_random_page() {
    if (has_pager()) {
        // Pick a random page
        const num_pages = get_num_pages();
        const random_page = get_random_page_index(num_pages);
        const url = new URL(window.location);
        url.searchParams.set('page', random_page);
        window.location.replace(url.href);
    }
}

/**
 * Randomly selects and returns an item from a listing.
 * @returns DOM node
 */
function get_random_item() {
    const items = document.querySelectorAll('.listing > *');

    items.forEach((item) => {
        item.classList.remove('randomly-selected');
    })

    const count = items.length;
    random_index = Math.floor(Math.random() * count);

    const random_item = items[random_index];
    return random_item;
}

/**
 * Write to session storage to indicate that we've started a selection
 */
function start_session() {
    sessionStorage.setItem('dndr-session', "true");
}

/**
 * Test to see if a session currently exists.
 * @returns true if a session exists, false otherwise.
 */
function session_exists() { 
    if (sessionStorage.getItem('dndr-session') == "true") {
        return true;
    }

    return false;
}

/**
 * Remove our session data.
 */
function end_session() {
    sessionStorage.clear();
}

/**
 * Button click handler
 */
function get_random() {
    const random_item = get_random_item();
    random_item.classList.add('randomly-selected');
    random_item.scrollIntoView();
}

function paged_get_random() {
    if(has_pager()) {
        start_session();
        goto_random_page();
    }
    else {
        const random_item = get_random_item();
        random_item.classList.add('randomly-selected');
        random_item.scrollIntoView();
    }
}

/**
 * Tests to see if there is a pager
 * @returns true if a pager exists, false otherwise.
 */
function has_pager() {
    const pager = document.querySelector('ul.paging-list')

    if (pager != null) {
        return true;
    } 

    return false;
}

/**
 * Inspect the pager to determine the number of pages in the listing.
 * @returns 
 */
function get_num_pages() {
    const pages = document.querySelectorAll('li.b-pagination-item');
    const last_page = pages.item(pages.length - 2);
    return parseInt(last_page.innerText);
}

/**
 * Picks a random page index between 1 and max
 * @param {*} max The maximum page index.
 * @returns a random integer
 */
function get_random_page_index(max) {
    const min = 1;
    const floatRandom = Math.random();
    const difference = max - min;
  
    // random between 0 and the difference
    const random = Math.round(difference * floatRandom);
    const randomWithinRange = random + min;
    return randomWithinRange;
}

/**
 * Test to see if we're on a page that the extension supports, according to the whitelist.
 * @returns true if we're on a supported page, false otherwise.
 */
function is_enabled() {
    let enabled = false;
    const path = window.location.pathname;

    whitelist_urls.forEach(pattern => {
        const re = new RegExp(pattern);
        
        if(re.test(path)) {
            enabled = true;
        }
    });

    return enabled;
}